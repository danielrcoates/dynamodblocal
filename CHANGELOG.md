# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 0.1.3 - 2018-07-17
### Fixed
- `CHANGELOG.md` version numbers :blush:

## 0.1.2 - 2018-07-17
### Fixed
- Naming the Vagrant Environment

## 0.1.1 - 2018-07-17
### Changed
- Updated the `Vagrantfile` to name the virtualbox, and hostname to `DynamoDBLocal`

## 0.1.0 - 2018-07-17
### Changed
- Renamed `README` to `README.md` and updated it
- Added the MIT licence
- Updated `Vagrantfile` to download, install and run AWS DynamoDB in the background

## 0.0.1 - 2018-07-17
Initial Project Setup

### Added
- `CHANGELOG.md`, `LICENCE`, `README.md`
- Initial `Vagrantfile`
