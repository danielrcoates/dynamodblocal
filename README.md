# DynamoDB Local
DynamoDB Local is a Vagrant VirtualBox, that will launch a local version of AWS DynamoDB.

* [Requirements](#requirements)
* [How to use](#how-to-use)
* [Data Storage](#data-storage)
* [FAQ's](#faqs)
* [Licence](#licence)
* [Changes](#changes)
* [TODO](#TODO)

## Requirements
* Vagrant
* VirtualBox

## How to use
AWS DynamoDB Local copies your local `.aws` config file to the virtual machine, if you don't have one you will need to create it, a template is bellow, this will need saving as `.aws` in your home directory.

```
AWS Access Key ID: "fakeMyKeyId"
AWS Secret Access Key: "fakeSecretAccessKey"
```
Then you can run the following commands in your favourite terminal app.

1. Clone this repo `git clone https:\\gitlab.com\danielcoates\dynamodblocal.git`
2. Change into the repo directory
3. Run `vagrant up`

You can now connect to your local AWS DynamoDB Service by setting the endpoint to `http:\\10.10.10.10:8000`

## Data Storage
This setup stores your data in the `data` folder, and will persist across runs, and will store all your data in a *shared database*, if you wish to use different AWS Regions then remove the `-sharedDb` flag from line `29` in the `Vagrantfile`.

## FAQ's

#### I'm using `10.10.10.10` somewhere else in my network, can I change it?
> Yes, just change the IP in the `Vagrantfile` to the one you wish to use, and then use the one you chose when you set your endpoint.

#### How do I set the endpoint?
> This depends on how your connecting to DynamoDB, but instructions for can be found in the [AWS DynamoDB Documentation](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.Endpoint.html)

## Licence
Released under the MIT licence

## Changes
Changes can be found in `CHANGELOG.md`

## TODO
* Create a way to migrate data to a live AWS DynamoDB.
* Create some kind of web interface to browse and manage data stored within the local environment,
