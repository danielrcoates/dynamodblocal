Vagrant.configure("2") do |config|
    # Set virtualbox as default enviroment
    config.vm.provider "virtualbox"

    # Set the name of the Vagrant Box
    config.vm.define "DynamoDBLocal"
    
    # Select the latest version of Ubuntu 16.04 LTS
    config.vm.box = "ubuntu/xenial64"

    # Prevent virtualbox from creating log files in /vagrant/ and set the name of the virtualbox
    config.vm.provider "virtualbox" do |vb|
        vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
        vb.name = "DynamoDBLocal"
    end

    # Set the name of the host machine
    config.vm.hostname = "DynamoDBLocal"

    # Create a private network, which allows host-only access to the machine
    # using 10.10.10.10
    config.vm.network "private_network", ip: "10.10.10.10"

    # Sync local .aws keys to the machine
    config.vm.provision "file", source: "~/.aws", destination: ".aws"

    # Provision the box and start the server.
    config.vm.provision "shell", inline: <<-SHELL
        apt-get update
        apt-get upgrade
        apt-get dist-upgrade
        apt-get install -y awscli default-jre tmux
        if [ ! -d /vagrant/data ]; then
            mkdir /vagrant/data
        fi
        wget -nv https://s3.eu-central-1.amazonaws.com/dynamodb-local-frankfurt/dynamodb_local_latest.tar.gz -O ~/dynamodb_local_latest.tar.gz
        tar -xzf ~/dynamodb_local_latest.tar.gz
        nohup java -Djava.library.path=/home/vagrant/DynamoDBLocal_lib -jar /home/vagrant/DynamoDBLocal.jar -sharedDb -dbPath /vagrant/data &
    SHELL
end
